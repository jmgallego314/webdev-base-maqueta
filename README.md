## ABBVIE RESTYLING


Proyecto base GULP usando [Nunjucks](http://mozilla.github.io/nunjucks/),[SASS](http://sass-lang.com/) y [Gulp](http://gulpjs.com/). Browsersync se ejecuta en la carpeta "dist" y monitoriza todas las plantillas Nunjucks, SASS y JS; recargando los cambios en los navegadores en tiempo real. Además optimiza las imágenes, genera fuentes basadas en iconos y mueve assets y fuentes a la carpeta "/dist" para un despliegue sencillo.



Tareas Gulp utilizadas:

- sass
- autoprefixer
- purifycss
- iconfont
- browserSync
- nunjucksRender
- script concat
- imagemin
- pngquant


## Setup

1) Instalar [Gulp](http://gulpjs.com/) y [NPM](http://nodejs.org) si todavía no lo tienes

2) Instalar dependencias

```
npm install
```

3) Ejecutar Gulp

```
gulp
```

4) Testing
Por defecto, los test se lanzan junto a los demás procesos de GULP, pero se pueden lanzar independientemente con las siguentes instrucciones:

```
gulp test-html
```

```
gulp test-css
```

Estos test generarán un reportes en las carpeta "dist/reports/".
