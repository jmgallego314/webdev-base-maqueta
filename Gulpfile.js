'use strict';


// -----------------------------------------------------------------------------
// Dependencies
// -----------------------------------------------------------------------------

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var nunjucksRender = require('gulp-nunjucks-render');
var concat      = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var sourcemaps = require('gulp-sourcemaps');
var purify = require('gulp-purifycss');

var access = require('gulp-accessibility');
var validate = require('gulp-w3c-css');


var consolidate = require('gulp-consolidate');

var fontName = 'fonticon';
var className = 'icon' // set class name in your CSS

var template = 'template-icons' // or 'foundation-style'

var siteOutput = './dist';


// -----------------------------------------------------------------------------
// Configuration
// -----------------------------------------------------------------------------

var input = './scss/**/*.scss';
var inputMain = './scss/main.scss';
var output = siteOutput + '/css';
var inputTemplates = './templates/**/*.html';
var inputPages = './templates/pages/*.html';
var sassOptions = { outputStyle: 'expanded' };
var autoprefixerOptions = { browsers: ['last 2 versions', '> 5%', 'Firefox ESR'] };


// -----------------------------------------------------------------------------
// Iconfont
// -----------------------------------------------------------------------------

gulp.task('iconfont', done => {
  gulp.src(['assets/icons/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: './scss/templates/_icons.scss',
      targetPath: '../../../scss/typography/_icons.scss',
      fontPath: '../fonts/icons/'
    }))
    .pipe(iconfont({
      fontName: fontName,
      formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
     })).on('glyphs', (glyphs) => {
          var options = {
            className,
            fontName,
            fontPath: '../fonts/icons/', // set path to font (from your CSS file if relative)
            glyphs: glyphs.map(mapGlyphs)
          }
        gulp.src('./assets/templates/icons.html')
          .pipe(consolidate('lodash', options))
          .pipe(gulp.dest('./dist')) // set path to export your sample HTML
        })
    .pipe(gulp.dest('./dist/fonts/icons/'));
    done();
});


// -----------------------------------------------------------------------------
// Sass compilation
// -----------------------------------------------------------------------------

gulp.task('sass', function() {
  return gulp
    .src(inputMain)
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(purify(['./js/**/*.js', inputPages, inputTemplates]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(output))
    .pipe(browserSync.stream());
});


// -----------------------------------------------------------------------------
// Javascriptd
// -----------------------------------------------------------------------------

gulp.task('scripts', function() {
  return gulp.src([
	  	'js/main.js'
  	])
    .pipe(concat({ path: 'main.js'}))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest(siteOutput + '/js'));
});


// -----------------------------------------------------------------------------
// Templating
// -----------------------------------------------------------------------------

gulp.task('nunjucks', function() {
  nunjucksRender.nunjucks.configure(['./templates/'],{
    trimBlocks: true,
    lstripBlocks: true
});
  // Gets .html and .nunjucks files in pages
  return gulp.src(inputPages)
  // Renders template with nunjucks
  .pipe(nunjucksRender({
    path: ['./templates/'],
    watch: true,
  }))
  // output files in dist folder
  .pipe(gulp.dest(siteOutput))
});


// -----------------------------------------------------------------------------
// Imagemin
// -----------------------------------------------------------------------------

gulp.task('img', function() {
  return gulp.src('./img/**/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(siteOutput + '/img'));
});


// -----------------------------------------------------------------------------
// Fonts
// -----------------------------------------------------------------------------

// gulp.task('fonts', function() {
//   return gulp.src(['./fonts/*'])
//   .pipe(gulp.dest(siteOutput + '/fonts/'));
// });


// -----------------------------------------------------------------------------
// Watchers
// -----------------------------------------------------------------------------

gulp.task('watch', function() {
    // Watch the sass input folder for change,
    // and run `sass` task when something happens
    gulp.watch(input, gulp.series('sass')).on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    gulp.watch('./js/*', gulp.series('scripts')).on('change', browserSync.reload);


    gulp.watch('./assets/icons/*.svg', gulp.series('iconfont'));

    // Watch nunjuck templates and reload browser if change
    gulp.watch(inputTemplates, gulp.series('nunjucks')).on('change', browserSync.reload);
    gulp.watch(inputPages, gulp.series('nunjucks')).on('change', browserSync.reload);

});


// -----------------------------------------------------------------------------
// Static server
// -----------------------------------------------------------------------------

gulp.task('browser-sync', function(done) {
  browserSync.init({
    server: {
      baseDir: siteOutput
    }
  });
  done();
});


// Accessibility levels WCAG2A, WCAG2AA, WCAG2AAA

gulp.task('test-html', function() {
  return gulp.src('./dist/**/*.html')
    .pipe(access({
      force: true,
      accessibilityLevel: 'WCAG2A',
      verbose: false
    }))
    .pipe(access.report({reportType: 'json'}))
    .pipe(rename({
      extname: '.log'
    }))
    .pipe(gulp.dest('./dist/reports/html'));
});


gulp.task('test-css', function() {
  return gulp.src('./dist/css/*.css')
    .pipe(validate())
    .pipe(rename({
      extname: '.log'
    }))
    .pipe(gulp.dest('./dist/reports/css'));
});


// -----------------------------------------------------------------------------
// Default task
// -----------------------------------------------------------------------------

gulp.task('default', gulp.series('iconfont', 'sass', 'nunjucks', 'img', 'scripts', 'browser-sync', 'test-html', 'test-css', 'watch'));


function mapGlyphs (glyph) {
  return { name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0) }
}
